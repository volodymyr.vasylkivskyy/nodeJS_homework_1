const express = require("express");
const app = express();
const fs = require("fs").promises;
const path = require("path");
const morgan = require("morgan");
app.use(morgan("common"));
app.use(express.json());
const files_dir = path.join(__dirname, "files");
const fsSync = require("fs");
if (!fsSync.existsSync("files")) {
  fsSync.mkdirSync("files");
}

app.post("/api/files", (req, res) => {
  const { filename, content } = req.body;
  const regexp = /\.(?:log|txt|json|yaml|xml|js)$/g;
  if (!filename) {
    return res
      .status(400)
      .json({ message: "Please specify 'filename' parameter" });
  }
  if (!content) {
    return res
      .status(400)
      .json({ message: "Please specify 'content' parameter" });
  }
  if (regexp.test(filename)) {
    fs.writeFile(path.join(files_dir, filename), content, function () {
      console.log("File created successfully");
    });
    res.status(200).json({ message: "File created successfully" });
  } else {
    res.status(400).json({ message: "Wrong ext!" });
  }
});

app.get("/api/files", async (req, res) => {
  const files = await fs.readdir("files");
  res.json({ message: "Success", files: files });
});

app.get("/api/files/:filename", async (req, res) => {
  const files = await fs.readdir("files");
  const reqFile = req.params.filename;

  if (files.indexOf(reqFile) === -1) {
    return res
      .status(400)
      .json({ message: `No file with '${reqFile}' filename found` });
  }

  const reqFileContent = await fs.readFile(`files/${reqFile}`, "utf-8");
  const reqFileExt = path.extname(`files/${reqFile}`).slice(1);
  const stats = await fs.stat(`files/${reqFile}`, function (err, stats) {
    if (err) throw err;
  });
  res.json({
    message: "Success",
    filename: reqFile,
    content: reqFileContent,
    extension: reqFileExt,
    uploadedDate: stats.mtime,
  });
});

app.put("/api/files/:filename", async (req, res) => {
  const files = await fs.readdir("files");
  const reqFile = req.params.filename;

  if (files.indexOf(reqFile) === -1) {
    return res
      .status(400)
      .json({ message: `No file with '${reqFile}' filename found` });
  }
  if (!req.body.newCont) {
    return res
      .status(400)
      .json({ message: "Please specify 'newContent' parameter" });
  }

  await fs.writeFile(
    `files/${reqFile}`,
    `${req.body.newCont}`,
    "utf-8",
    (err) => {
      if (err) throw err;
    }
  );

  res.json({ message: "File is modified" });
});

app.use(function (err, req, res, next) {
  console.error(err.stack);
  res.status(500).json({ message: "Server error" });
});

app.listen(8080, () => {
  console.log("Server is working on 8080");
});
